#!/bin/bash

export PATH=$PATH:/bin

SERVER=""
STARTWHILECHECK=""

function screen() {
  /usr/bin/script -q -c "/usr/bin/screen ${*}" /dev/null
}

IsStarted() {
	if ! sudo -u root screen -list | grep -q "$SERVER"
	then
		return 1
	else
		return 0
	fi
}

SetConfigFile() {
	. /mcLang.cfg
}

IsStartedCheckServerStart() {
	netstat -tulpn | grep -q $PORT
	testingPort="$?"
	if [ "$testingPort" != 0 ]; then
		STARTWHILECHECK="false"
	else
		STARTWHILECHECK="true"
	fi
}

case "$1" in
	console)
		case "$2" in
			bungee)
				SERVER="bungee"
				SetConfigFile
				echo -e "\e[93m$CHECKSERVERSTATUS\e[39m"
				if ( IsStarted )
				then
					echo -e "\e[36m$REDIRECTTOCONSOLE \e[92m[\e[34mscreen -x $SERVER\e[92m]\e[39m"
					echo -e "\e[93m$LEAVECONSOLEINFO"
					sleep 2
					sudo -u root screen -x $SERVER
				else
					echo -e "\e[101m\e[93m$SERVEROFF"
				fi
			;;
			survival)
				SERVER="survival"
				SetConfigFile
				echo -e "\e[93m$CHECKSERVERSTATUS\e[39m"
				if ( IsStarted )
				then
					echo -e "\e[36m$REDIRECTTOCONSOLE \e[92m[\e[34mscreen -x $SERVER\e[92m]\e[39m"
					echo -e "\e[93m$LEAVECONSOLEINFO"
					sleep 2
					sudo -u root screen -x $SERVER
				else
					echo -e "\e[101m\e[93m$SERVEROFF"
				fi
			;;
			build)
				SERVER="build"
				SetConfigFile
				echo -e "\e[93m$CHECKSERVERSTATUS\e[39m"
				if ( IsStarted )
				then
					echo -e "\e[36m$REDIRECTTOCONSOLE \e[92m[\e[34mscreen -x $SERVER\e[92m]\e[39m"
					echo -e "\e[93m$LEAVECONSOLEINFO"
					sleep 2
					sudo -u root screen -x $SERVER
				else
					echo -e "\e[101m\e[93m$SERVEROFF"
				fi
			;;
			*)
				SetConfigFile
				echo -e "\e[93m$COMMANDCONSOLE\e[39m"
			;;
		esac
	;;
	PurgeCache)
		RAMPURGE=`awk '/Mem:/ {print $6}' <(free -h) | cut -d 'G' -f 1 | cut -d 'M' -f 1 | tr -s '.' ','`
		RAMCHECK="01,5"
		if [[ "$RAMCHECK" -ge "$RAMPURGE" ]]; then
			SetConfigFile
			echo -e "\e[93m$COMMANDEPURGECACHEPURGE\e[39m"
			echo 3 > /proc/sys/vm/drop_caches
			echo -e "\e[93m$COMMANDEPURGECACHEPURGED`awk '/Mem:/ {print $6}' <(free -h) | cut -d 'i' -f 1`\e[39m"

		else
			SetConfigFile
			echo -e "\e[93m$COMMANDEPURGECACHEPURGEDTWO`awk '/Mem:/ {print $6}' <(free -h) | cut -d 'i' -f 1`\e[39m"
		fi
	;;
	removelogs)
		rm -f /home/servers/bungee/logs/*.log.gz
		for list in 'survival' 'build'
		do
			rm -f /home/servers/$list/logs/*.log.gz
			rm -f /home/servers/$list/plugins/Skript/backups/*.csv
			rm -f /home/servers/$list/.console_history
		done
	;;
	restart)
		case "$2" in
			all)
				echo -e "################################ RESTARTING PROCESS ################################"
				mc stop bungee
				sleep 0.5
				mc stop survival
				sleep 0.5
				mc stop build
				sleep 2
				mc start survival
				sleep 0.5
				mc start build
				sleep 0.5
				mc start bungee
				echo -e "####################################################################################"
			;;
			bungee)
				echo -e "################################ RESTARTING PROCESS ################################"
				mc stop bungee
				sleep 3
				mc start bungee
				echo -e "####################################################################################"
			;;
			survival)
				echo -e "################################ RESTARTING PROCESS ################################"
				mc stop survival
				sleep 3
				mc start survival
				echo -e "####################################################################################"
			;;
			build)
				echo -e "################################ RESTARTING PROCESS ################################"
				mc stop build
				sleep 3
				mc start build
				echo -e "####################################################################################"
			;;
			*)
				SetConfigFile
				echo -e "\e[93m$COMMANDRESTART\e[39m"
			;;
		esac
	;;
	save)
		SetConfigFile
		case "$2" in
			all)
				mc save build
				mc save survival
				mc save bungee
			;;
			bungee)
				SERVER="bungee"
				SetConfigFile
				if ( IsStarted )
				then
					screen -S $SERVER -X stuff "alert $SAVEALERT\r"
					mc stop $SERVER
					if [ -d /opt/temp/ ];then
						echo -e "$TEMPDIRALREADYCREATED"
					else
						echo -e "\e[36m$CREATETEMPDIR \e[92m[\e[34mmkdir /opt/temp/\e[92m]\e[39m"
						mkdir /opt/temp/
					fi
					echo -e "\e[36m$DIRMOVE \e[92m[\e[34mcd /home/servers/$SERVER/\e[92m]\e[39m"
					cd /home/servers/$SERVER/
					echo -e "\e[36m$COPYSERVERTOTEMPDIR \e[92m[\e[34mcp -r /home/servers/$SERVER/ /opt/temp/\e[92m]\e[39m"
					cp -r /home/servers/$SERVER/ /opt/temp/
					echo -e "\e[36m$COMPRESSSERVER \e[92m[\e[34mzip -r /home/saves/$SERVER/$DATESAVE.zip /opt/temp/\e[92m]\e[39m"
					zip -r /home/saves/$SERVER/$DATESAVE.zip /opt/temp/
					echo -e "\e[36m$DELETETEMPDIR \e[92m[\e[34mrm -fr /opt/temp/\e[92m]\e[39m"
					rm -fr /opt/temp/
					mc start $SERVER
				else
					echo -e "\e[93m$CHECKSERVERSTATUS\e[39m"
					if [ -d /opt/temp/ ];then
						echo -e "$TEMPDIRALREADYCREATED"
					else
						echo -e "\e[36m$CREATETEMPDIR \e[92m[\e[34mmkdir /opt/temp/\e[92m]\e[39m"
						mkdir /opt/temp/
					fi
					echo -e "\e[36m$DIRMOVE \e[92m[\e[34mcd /home/servers/$SERVER/\e[92m]\e[39m"
					cd /home/servers/$SERVER/
					echo -e "\e[36m$COPYSERVERTOTEMPDIR \e[92m[\e[34mcp -r /home/servers/$SERVER/ /opt/temp/\e[92m]\e[39m"
					cp -r /home/servers/$SERVER/ /opt/temp/
					echo -e "\e[36m$COMPRESSSERVER \e[92m[\e[34mzip -r /home/saves/$SERVER/$DATESAVE.zip /opt/temp/\e[92m]\e[39m"
					zip -r /home/saves/$SERVER/$DATESAVE.zip /opt/temp/
					echo -e "\e[36m$DELETETEMPDIR \e[92m[\e[34mrm -fr /opt/temp/\e[92m]\e[39m"
					rm -fr /opt/temp/
				fi
			;;
			survival)
				SERVER="survival"
				SetConfigFile
				if ( IsStarted )
				then
					screen -S $SERVER -X stuff "say $SAVEALERT\r"
					mc stop $SERVER
					if [ -d /opt/temp/ ];then
						echo -e "$TEMPDIRALREADYCREATED"
					else
						echo -e "\e[36m$CREATETEMPDIR \e[92m[\e[34mmkdir /opt/temp/\e[92m]\e[39m"
						mkdir /opt/temp/
					fi
					echo -e "\e[36m$DIRMOVE \e[92m[\e[34mcd /home/servers/$SERVER/\e[92m]\e[39m"
					cd /home/servers/$SERVER/
					echo -e "\e[36m$COPYSERVERTOTEMPDIR \e[92m[\e[34mcp -r /home/servers/$SERVER/ /opt/temp/\e[92m]\e[39m"
					cp -r /home/servers/$SERVER/ /opt/temp/
					echo -e "\e[36m$COMPRESSSERVER \e[92m[\e[34mzip -r /home/saves/$SERVER/$DATESAVE.zip /opt/temp/\e[92m]\e[39m"
					zip -r /home/saves/$SERVER/$DATESAVE.zip /opt/temp/
					echo -e "\e[36m$DELETETEMPDIR \e[92m[\e[34mrm -fr /opt/temp/\e[92m]\e[39m"
					rm -fr /opt/temp/
					mc start $SERVER
				else
					echo -e "\e[93m$CHECKSERVERSTATUS\e[39m"
					if [ -d /opt/temp/ ];then
						echo -e "$TEMPDIRALREADYCREATED"
					else
						echo -e "\e[36m$CREATETEMPDIR \e[92m[\e[34mmkdir /opt/temp/\e[92m]\e[39m"
						mkdir /opt/temp/
					fi
					echo -e "\e[36m$DIRMOVE \e[92m[\e[34mcd /home/servers/$SERVER/\e[92m]\e[39m"
					cd /home/servers/$SERVER/
					echo -e "\e[36m$COPYSERVERTOTEMPDIR \e[92m[\e[34mcp -r /home/servers/$SERVER/ /opt/temp/\e[92m]\e[39m"
					cp -r /home/servers/$SERVER/ /opt/temp/
					echo -e "\e[36m$COMPRESSSERVER \e[92m[\e[34mzip -r /home/saves/$SERVER/$DATESAVE.zip /opt/temp/\e[92m]\e[39m"
					zip -r /home/saves/$SERVER/$DATESAVE.zip /opt/temp/
					echo -e "\e[36m$DELETETEMPDIR \e[92m[\e[34mrm -fr /opt/temp/\e[92m]\e[39m"
					rm -fr /opt/temp/
				fi
			;;
			build)
				SERVER="build"
				SetConfigFile
				if ( IsStarted )
				then
					screen -S $SERVER -X stuff "say $SAVEALERT\r"
					mc stop $SERVER
					if [ -d /opt/temp/ ];then
						echo -e "$TEMPDIRALREADYCREATED"
					else
						echo -e "\e[36m$CREATETEMPDIR \e[92m[\e[34mmkdir /opt/temp/\e[92m]\e[39m"
						mkdir /opt/temp/
					fi
					echo -e "\e[36m$DIRMOVE \e[92m[\e[34mcd /home/servers/$SERVER/\e[92m]\e[39m"
					cd /home/servers/$SERVER/
					echo -e "\e[36m$COPYSERVERTOTEMPDIR \e[92m[\e[34mcp -r /home/servers/$SERVER/ /opt/temp/\e[92m]\e[39m"
					cp -r /home/servers/$SERVER/ /opt/temp/
					echo -e "\e[36m$COMPRESSSERVER \e[92m[\e[34mzip -r /home/saves/$SERVER/$DATESAVE.zip /opt/temp/\e[92m]\e[39m"
					zip -r /home/saves/$SERVER/$DATESAVE.zip /opt/temp/
					echo -e "\e[36m$DELETETEMPDIR \e[92m[\e[34mrm -fr /opt/temp/\e[92m]\e[39m"
					rm -fr /opt/temp/
					mc start $SERVER
				else
					echo -e "\e[93m$CHECKSERVERSTATUS\e[39m"
					if [ -d /opt/temp/ ];then
						echo -e "$TEMPDIRALREADYCREATED"
					else
						echo -e "\e[36m$CREATETEMPDIR \e[92m[\e[34mmkdir /opt/temp/\e[92m]\e[39m"
						mkdir /opt/temp/
					fi
					echo -e "\e[36m$DIRMOVE \e[92m[\e[34mcd /home/servers/$SERVER/\e[92m]\e[39m"
					cd /home/servers/$SERVER/
					echo -e "\e[36m$COPYSERVERTOTEMPDIR \e[92m[\e[34mcp -r /home/servers/$SERVER/ /opt/temp/\e[92m]\e[39m"
					cp -r /home/servers/$SERVER/ /opt/temp/
					echo -e "\e[36m$COMPRESSSERVER \e[92m[\e[34mzip -r /home/saves/$SERVER/$DATESAVE.zip /opt/temp/\e[92m]\e[39m"
					zip -r /home/saves/$SERVER/$DATESAVE.zip /opt/temp/
					echo -e "\e[36m$DELETETEMPDIR \e[92m[\e[34mrm -fr /opt/temp/\e[92m]\e[39m"
					rm -fr /opt/temp/
				fi
			;;
			*)
				echo -e "\e[93m$COMMANDSAVE\e[39m"
			;;
		esac
	;;
	start)
		case "$2" in
			all)
				mc start bungee
				mc start survival
				mc start build
			;;
			bungee)
				SERVER="bungee"
				PORT="25565"
				SetConfigFile
				echo -e "\e[93m$CHECKSERVERSTATUS\e[39m"
				if ( IsStarted )
				then
					echo -e "\e[101m\e[97m$SERVERSTARTEDEXEC\e[39m\e[49m"
				else
					echo -e "\e[36m$DIRMOVE \e[92m[\e[34mcd /home/servers/$SERVER/\e[92m]\e[39m"
					cd /home/servers/$SERVER/
					echo -e "\e[36m$STARTINGSERVER \e[92m[\e[34mscreen -dmS $SERVER java $RAMBUNGEE -jar waterfall.jar\e[92m]\e[39m"
					sudo -u root screen -dmS $SERVER java $RAMBUNGEE -jar waterfall.jar
					IsStartedCheckServerStart
					if [ "$STARTWHILECHECK" == "false" ]
					then
						while [ "$STARTWHILECHECK" == "false" ]
						do
							IsStartedCheckServerStart
								if [ "$STARTWHILECHECK" == "true" ]
								then
									echo -e "\e[92m$SERVERON\e[39m"
								fi
						done
					else
						echo -e "\e[92m$SERVERALREADYON\e[39m"
					fi
				fi
			;;
			survival)
				SERVER="survival"
				PORT="25566"
				SetConfigFile
				echo -e "\e[93m$CHECKSERVERSTATUS\e[39m"
				if ( IsStarted )
				then
					echo -e "\e[101m\e[97m$SERVERSTARTEDEXEC\e[39m\e[49m"
				else
					echo -e "\e[36m$DIRMOVE \e[92m[\e[34mcd /home/servers/$SERVER/\e[92m]\e[39m"
					cd /home/servers/$SERVER/
					echo -e "\e[36m$STARTINGSERVER \e[92m[\e[34mscreen -dmS $SERVER java $RAMSURVIVAL -jar paper.jar\e[92m]\e[39m"
					sudo -u root screen -dmS $SERVER java $RAMSURVIVAL -jar paper.jar
					IsStartedCheckServerStart
					if [ "$STARTWHILECHECK" == "false" ]
					then
						while [ "$STARTWHILECHECK" == "false" ]
						do
							IsStartedCheckServerStart
								if [ "$STARTWHILECHECK" == "true" ]
								then
									echo -e "\e[92m$SERVERON\e[39m"
								fi
						done
					else
						echo -e "\e[92m$SERVERALREADYON\e[39m"
					fi
				fi
			;;
			build)
				SERVER="build"
				PORT="25567"
				SetConfigFile
				echo -e "\e[93m$CHECKSERVERSTATUS\e[39m"
				if ( IsStarted )
				then
					echo -e "\e[101m\e[97m$SERVERSTARTEDEXEC\e[39m\e[49m"
				else
					echo -e "\e[36m$DIRMOVE \e[92m[\e[34mcd /home/servers/$SERVER/\e[92m]\e[39m"
					cd /home/servers/$SERVER/
					echo -e "\e[36m$STARTINGSERVER \e[92m[\e[34mscreen -dmS $SERVER java $RAMBUILD -jar paper.jar\e[92m]\e[39m"
					sudo -u root screen -dmS $SERVER java $RAMBUILD -jar paper.jar
					IsStartedCheckServerStart
					if [ "$STARTWHILECHECK" == "false" ]
					then
						while [ "$STARTWHILECHECK" == "false" ]
						do
							IsStartedCheckServerStart
								if [ "$STARTWHILECHECK" == "true" ]
								then
									echo -e "\e[92m$SERVERON\e[39m"
								fi
						done
					else
						echo -e "\e[92m$SERVERALREADYON\e[39m"
					fi
				fi
			;;
			*)
				SetConfigFile
				echo -e "\e[93m$COMMANDSTART\e[39m"
			;;
		esac
	;;
	status)
		SetConfigFile
		case "$2" in
			all)
				mc status bungee
				mc status survival
				mc status build
			;;
			bungee)
				SERVER="bungee"
				PORT="25565"
				SetConfigFile
				echo -e "\e[93m$CHECKSERVERSTATUS\e[39m"
				if ( IsStarted )
				then
					IsStartedCheckServerStart
					if [ "$STARTWHILECHECK" == "true" ]
					then
						echo -e "\e[92m$SERVERON\e[39m"
					else
						echo -e "\e[101m\e[97m$SCREENCREATEDSERVEROFF\e[39m\e[49m"
					fi
				else
					echo -e "\e[101m\e[97m$SCREENNONEXISTENT\e[39m\e[49m"
				fi
			;;
			survival)
				SERVER="survival"
				PORT="25566"
				SetConfigFile
				echo -e "\e[93m$CHECKSERVERSTATUS\e[39m"
				if ( IsStarted )
				then
					IsStartedCheckServerStart
					if [ "$STARTWHILECHECK" == "true" ]
					then
						echo -e "\e[92m$SERVERON\e[39m"
					else
						echo -e "\e[101m\e[97m$SCREENCREATEDSERVEROFF\e[39m\e[49m"
					fi
				else
					echo -e "\e[101m\e[97m$SCREENNONEXISTENT\e[39m\e[49m"
				fi
			;;
			build)
				SERVER="build"
				PORT="25567"
				SetConfigFile
				echo -e "\e[93m$CHECKSERVERSTATUS\e[39m"
				if ( IsStarted )
				then
					IsStartedCheckServerStart
					if [ "$STARTWHILECHECK" == "true" ]
					then
						echo -e "\e[92m$SERVERON\e[39m"
					else
						echo -e "\e[101m\e[97m$SCREENCREATEDSERVEROFF\e[39m\e[49m"
					fi
				else
					echo -e "\e[101m\e[97m$SCREENNONEXISTENT\e[39m\e[49m"
				fi
			;;
			*)
				SetConfigFile
				echo -e "\e[93m$COMMANDSTATUS\e[39m"
			;;
		esac
	;;
	stop)
		case "$2" in
			all)
				mc stop bungee
				mc stop survival
				mc stop build
			;;
			bungee)
				SERVER="bungee"
				PORT="25565"
				SetConfigFile
				echo -e "\e[93m$CHECKSERVERSTATUS\e[39m"
				if ( IsStarted )
				then
					echo -e "\e[36m$SHUTINGDOWNSERVER \e[92m[\e[34mscreen -S $SERVER -X stuff ""end""\e[92m]\e[39m"
					sudo -u root screen -S $SERVER -X stuff "end\r"
					IsStartedCheckServerStart
					if [ "$STARTWHILECHECK" == "true" ]
					then
						while [ "$STARTWHILECHECK" == "true" ]
						do
							IsStartedCheckServerStart
							if [ "$STARTWHILECHECK" == "false" ]; then
								if ( IsStarted )
								then
									echo -e "\e[92m$SERVEROFF\e[39m"
								fi
							fi
						done
					else
						echo -e "\e[92m$SERVERALREADYOFF\e[39m"
					fi
				else
					echo -e "\e[92m$SERVERALREADYOFF\e[39m"
				fi
			;;
			survival)
				SERVER="survival"
				PORT="25566"
				SetConfigFile
				echo -e "\e[93m$CHECKSERVERSTATUS\e[39m"
				if ( IsStarted )
				then
					echo -e "\e[36m$SHUTINGDOWNSERVER \e[92m[\e[34mscreen -S $SERVER -X stuff ""stop""\e[92m]\e[39m"
					sudo -u root screen -S $SERVER -X stuff "save-all\r"
					sleep 0.2
					sudo -u root screen -S $SERVER -X stuff "stop\r"
					IsStartedCheckServerStart
					if [ "$STARTWHILECHECK" == "true" ]
					then
						while [ "$STARTWHILECHECK" == "true" ]
						do
							IsStartedCheckServerStart
							if [ "$STARTWHILECHECK" == "false" ]; then
								if ( IsStarted )
								then
									echo -e "\e[92m$SERVEROFF\e[39m"
								fi
							fi
						done
					else
						echo -e "\e[92m$SERVERALREADYOFF\e[39m"
					fi
				else
					echo -e "\e[92m$SERVERALREADYOFF\e[39m"
				fi
			;;
			build)
				SERVER="build"
				PORT="25567"
				SetConfigFile
				echo -e "\e[93m$CHECKSERVERSTATUS\e[39m"
				if ( IsStarted )
				then
					echo -e "\e[36m$SHUTINGDOWNSERVER \e[92m[\e[34mscreen -S $SERVER -X stuff ""stop""\e[92m]\e[39m"
					sudo -u root screen -S $SERVER -X stuff "save-all\r"
					sleep 0.2
					sudo -u root screen -S $SERVER -X stuff "stop\r"
					IsStartedCheckServerStart
					if [ "$STARTWHILECHECK" == "true" ]
					then
						while [ "$STARTWHILECHECK" == "true" ]
						do
							IsStartedCheckServerStart
							if [ "$STARTWHILECHECK" == "false" ]; then
								if ( IsStarted )
								then
									echo -e "\e[92m$SERVEROFF\e[39m"
								fi
							fi
						done
					else
						echo -e "\e[92m$SERVERALREADYOFF\e[39m"
					fi
				else
					echo -e "\e[92m$SERVERALREADYOFF\e[39m"
				fi
			;;
			*)
				SetConfigFile
				echo -e "\e[93m$COMMANDSTOP\e[39m"
			;;
		esac
	;;
	*)
		SetConfigFile
		echo -e "\e[93m$COMMANDS\e[39m"
		echo -e "\e[93m$SUBCOMMANDS\e[39m"
		echo -e "\e[93m$SERVERSSIZE `du -sh /home/servers/`\e[39m"
		echo -e "\e[93m$SAVESSIZE `du -sh /home/saves/`\e[39m"
	;;
esac